<?php

namespace LearnyBox\Exception;

/**
 * Class InvalidTokenException
 * @package LearnyBox\Exception
 */
class InvalidTokenException extends LearnyBoxException
{

}