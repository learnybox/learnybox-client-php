<?php

namespace LearnyBox\Exception;

/**
 * Class InvalidApiKeyException
 * @package LearnyBox\Exception
 */
class InvalidApiKeyException extends LearnyBoxException
{

}