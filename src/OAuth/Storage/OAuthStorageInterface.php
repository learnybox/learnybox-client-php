<?php

namespace LearnyBox\OAuth\Storage;

/**
 * Interface OAuthStorageInterface
 * @package LearnyBox\OAuth\Storage
 */
interface OAuthStorageInterface
{

    public function getAccessToken();
    public function setAccessToken($accessToken);

    public function getRefreshToken();
    public function setRefreshToken($refreshToken);

}