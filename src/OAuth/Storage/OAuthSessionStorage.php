<?php

namespace LearnyBox\OAuth\Storage;

/**
 * Class OAuthSessionStorage
 * @package LearnyBox\OAuth\Storage
 */
class OAuthSessionStorage implements OAuthStorageInterface
{

    const ACCESS_TOKEN_INDEX = 'learnybox_oauth_access_token';
    const REFRESH_TOKEN_INDEX = 'learnybox_oauth_refresh_token';

    /**
     * OAuth constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return isset($_SESSION[self::ACCESS_TOKEN_INDEX]) ? $_SESSION[self::ACCESS_TOKEN_INDEX] : null;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $_SESSION[self::ACCESS_TOKEN_INDEX] = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getRefreshToken()
    {
        return isset($_SESSION[self::REFRESH_TOKEN_INDEX]) ? $_SESSION[self::REFRESH_TOKEN_INDEX] : null;
    }

    /**
     * @param mixed $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $_SESSION[self::REFRESH_TOKEN_INDEX] = $refreshToken;
    }

}