<?php

namespace LearnyBox\Resource;

use LearnyBox\Client;
use GuzzleHttp\Exception\GuzzleException;
use LearnyBox\Exception\InvalidApiKeyException;
use LearnyBox\Exception\InvalidTokenException;
use LearnyBox\Exception\LearnyBoxException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class AbstractResource
 * @package LearnyBox\Resource
 */
abstract class AbstractResource
{

    /**
     * @var Client
     */
    private $client;

    /**
     * AbstractResource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws GuzzleException
     * @throws InvalidApiKeyException
     */
    protected function authenticate()
    {
        $headers = [];

        $headers['X-API-Key'] = $this->client->getConfig('api_key');
        $data = ['grant_type' => 'access_token'];

        $this->sendAuthenticationRequest($data, $headers);
    }

    /**
     * @throws GuzzleException
     * @throws InvalidApiKeyException
     */
    public function refreshAuthentication()
    {
        $OAuthStorage = $this->client->getOAuthStorage();

        $data = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $OAuthStorage->getRefreshToken()
        ];

        $this->sendAuthenticationRequest($data);
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return null !== $this->client->getOAuthStorage()->getAccessToken() ? true : false;
    }

    /**
     * @param $method
     * @param $route
     * @param null $data
     * @param array $headers
     * @return mixed
     * @throws InvalidTokenException
     * @throws LearnyBoxException
     * @throws GuzzleException
     */
    public function sendRequest($method, $route, $data = null, array $headers = [])
    {
        $options = [];

        $options['headers'] = $headers;
        $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
        $options['headers']['Accept'] = 'application/json';

        if (is_array($data)) {
            $options['form_params'] = $data;
        }

        $response = $this->client->getHttpClient()->request($method, $route, $options);

        return $this->handleResponse($response);
    }

    /**
     * @param $method
     * @param $route
     * @param null $data
     * @param array $headers
     * @return mixed
     * @throws GuzzleException
     * @throws LearnyBoxException
     */
    public function sendRequestWithAuthorization($method, $route, $data = null, array $headers = [])
    {
        if (!$this->isAuthenticated()) {
            $this->authenticate();
        }

        try {
            $headers['Authorization'] = 'Bearer ' . $this->client->getOAuthStorage()->getAccessToken();

            return $this->sendRequest($method, $route, $data, $headers);
        } catch (InvalidTokenException $e) {
            $this->refreshAuthentication();
            return $this->sendRequestWithAuthorization($method, $route, $data, $headers);
        }
    }

    /**
     * @param null $data
     * @param array $headers
     * @throws GuzzleException
     * @throws InvalidApiKeyException
     */
    public function sendAuthenticationRequest($data = null, array $headers = [])
    {
        $OAuthStorage = $this->client->getOAuthStorage();

        try {
            $response = $this->sendRequest('POST', 'oauth/token/', $data, $headers);

            if ($response->status) {
                $OAuthStorage->setAccessToken($response->data->access_token);
                $OAuthStorage->setRefreshToken($response->data->refresh_token);
            }
        } catch (LearnyBoxException $e) {
            throw new InvalidApiKeyException();
        }
    }

    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws InvalidTokenException
     * @throws LearnyBoxException
     */
    public function handleResponse(ResponseInterface $response)
    {
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();

        $body = null;
        try {
            $body = json_decode($response->getBody()->getContents());
        } catch (\Exception $e) {
        }

        $message = null !== $body && isset($body->message)? $body->message : $reason;

        if ($code >= 200 && $code < 300) {
            return $body;
        } elseif ($code === 498) {
            throw new InvalidTokenException($message, $code);
        } else {
            throw new LearnyBoxException($message, $code);
        }
    }

}