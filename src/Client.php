<?php

namespace LearnyBox;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface;
use LearnyBox\OAuth\Storage\OAuthSessionStorage;
use LearnyBox\OAuth\Storage\OAuthStorageInterface;
use LearnyBox\Resource\AbstractResource;
use LearnyBox\Resource\DefaultResource;
use LearnyBox\Resource\Formations;
use LearnyBox\Resource\FormationsCommentaires;
use GuzzleHttp\Exception\GuzzleException;
use Exception;

/**
 * Class Client
 * @package LearnyBox
 */
class Client
{

    const DEFAULT_API_ENTRYPOINT = '/api/v2/';
    const DEFAULT_BASE_URL = 'learnybox.com';
    const DEFAULT_PROTOCOL = 'https';

    /**
     * @var ClientInterface $httpClient;
     */
    private $httpClient;

    /**
     * @var OAuthStorageInterface
     */
    private $OAuthStorage;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var DefaultResource
     */
    private $defaultResource;

    /**
     * Client constructor.
     * @param array $config
     * @param OAuthStorageInterface|null $OAuthStorage
     */
    public function __construct(array $config, OAuthStorageInterface $OAuthStorage = null)
    {
        $this->config = array_merge(
            [
                'api_entrypoint' => self::DEFAULT_API_ENTRYPOINT,
                'api_key' => null,
                'base_url' => self::DEFAULT_BASE_URL,
                'protocol' => self::DEFAULT_PROTOCOL,
                'subdomain' => '',
                'timeout' => 60.0
            ],
            $config
        );

        $this->httpClient = $this->createHttpClient();

        if (null === $OAuthStorage) {
            $this->OAuthStorage = $this->createOAuthStorage();
        }
    }

    /**
     * @param array $config
     * @param OAuthStorageInterface|null $OAuthStorage
     * @return Client
     */
    public static function create(array $config, OAuthStorageInterface $OAuthStorage = null)
    {
        return new static($config, $OAuthStorage);
    }

    /**
     * @return HttpClient
     */
    protected function createHttpClient()
    {
        $baseUri = implode('', [
            $this->config['protocol'] . '://',
            !empty($this->config['subdomain']) ? $this->config['subdomain'] . '.' : null,
            $this->config['base_url'],
            $this->config['api_entrypoint']
        ]);

        $options = [
            'exceptions' => false,
            'base_uri' => $baseUri,
            'timeout'  => $this->config['timeout'],
        ];

        return new HttpClient($options);
    }

    /**
     * @return OAuthStorageInterface
     */
    protected function createOAuthStorage()
    {
        return new OAuthSessionStorage();
    }

    /**
     * @param ClientInterface $httpClient
     */
    public function setHttpClient(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return ClientInterface
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @return OAuthStorageInterface
     */
    public function getOAuthStorage()
    {
        return $this->OAuthStorage;
    }

    /**
     * @param OAuthStorageInterface $OAuthStorage
     */
    public function setOAuthStorage($OAuthStorage)
    {
        $this->OAuthStorage = $OAuthStorage;
    }

    /**
     * @param null $parameter
     * @return array
     */
    public function getConfig($parameter = null)
    {
        if (null !== $parameter && isset($this->config[$parameter])) {
            return $this->config[$parameter];
        }

        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $method
     * @param $route
     * @param null $data
     * @param array $headers
     * @return mixed
     * @throws Exception\LearnyBoxException
     * @throws GuzzleException
     */
    public function send($method, $route, $data = null, array $headers = [])
    {
        $resource = null !== $this->defaultResource ? $this->defaultResource : $this->getResource('default');

        return $resource->sendRequestWithAuthorization($method, $route, $data, $headers);
    }

    /**
     * @param string $route
     * @return mixed
     * @throws GuzzleException
     */
    public function get($route)
    {
        return $this->send('GET', $route);
    }

    /**
     * @param string $route
     * @param array $data
     * @return mixed
     * @throws GuzzleException
     */
    public function post($route, $data)
    {
        return $this->send('POST', $route, $data);
    }

    /**
     * @param string $route
     * @param array $data
     * @return mixed
     * @throws GuzzleException
     */
    public function patch($route, $data)
    {
        return $this->send('PATCH', $route, $data);
    }

    /**
     * @param string $route
     * @return mixed
     * @throws GuzzleException
     */
    public function delete($route)
    {
        return $this->send('DELETE', $route);
    }

    /**
     * @param $resourceName
     * @return AbstractResource
     * @throws Exception
     */
    public function getResource($resourceName)
    {
        if ((array_key_exists($resourceName, $resources = $this->getResources()))) {
            $className = $resources[$resourceName];
            $resource = new $className($this);

            return $resource;
        } else {
            throw new \Exception('Resource not found');
        }
    }

    /**
     * @return array
     */
    protected function getResources()
    {
        return [
            'default' => DefaultResource::class
        ];
    }

}