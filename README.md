# LearnyBox API Client for PHP

The LearnyBox API Client for PHP provides convenient access to the LearnyBox API from
applications written in the PHP language. It includes a pre-defined set of
classes for API resources that initialize themselves dynamically from API
responses which makes it compatible with a wide range of versions of the LearnyBox
API.

## Requirements

* PHP 7.1.0 and later
* [Guzzle](https://github.com/guzzle/guzzle) library
* (optional) [PHPUnit](https://phpunit.de) to run tests

## Installation

You can use **Composer** or simply **Manual Installation**

### Composer

The preferred method is via [composer](https://getcomposer.org/). Follow the
[installation instructions](https://getcomposer.org/doc/00-intro.md) if you do not already have
composer installed.

Once composer is installed, execute the following command in your project root to install this library:

```bash
composer require learnybox/learnybox-client-php
```

Finally, be sure to include the autoloader:

```php
require_once('vendor/autoload.php');
```

### Manual Installation

If you abhor using composer, you can download the package in its entirety. The [Releases](https://gitlab.com/learnybox/learnybox-client-php/-/tags) page lists all stable versions. Download any file
with the name `learnybox-client-php-[RELEASE_NAME].zip` for a package including this library and its dependencies.

Uncompress the zip file you download, and include the autoloader in your project:

```php
require_once('/path/to/learnybox-client-php/vendor/autoload.php');
```

## Basic usage

```php
// load Composer
require('vendor/autoload.php');

use LearnyBox\Client;

$client = Client::create([
    'api_key' => 'abc123',
    'subdomain' => 'my-subdomain'
]);

$resultContacts = $client->get('mail/contacts/');

$resultNewContact = $client->post('mail/contacts/', [
    'nom' => 'Doe',
    'prenom' => 'John',
    'email' => 'john.doe@mail.com'
]);
$contactId = $resultNewContact->data->id_contact;

$resultEditContact = $client->patch('mail/contacts/', [
    'id_contact' => $contactId,
    'new_email' => 'john.doe.bis@mail.com',
]);
        
$resultContact = $client->get('mail/contacts/' . $contactId . '/');

$resultDelete = $client->delete('mail/contacts/' . $contactId . '/');
```

The `$client` object gives you access to the entire LearnyBox API.

## Caching

It is recommended to use another caching library to improve performance. This can be done by passing a [PSR-6](https://www.php-fig.org/psr/psr-6/) compatible library to the client:

```php
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

$filesystemAdapter = new Local(__DIR__.'/');
$filesystem        = new Filesystem($filesystemAdapter);

$cache = new FilesystemCachePool($filesystem);
$client->setCache($cache);
```

In this example we use [PHP Cache](http://www.php-cache.com/). Add this to your project with composer:

```
composer require cache/filesystem-adapter
```

## Development

Install dependencies:

```bash
composer install
```

Copy and configure phpunit.xml file:

```bash
cp ./phpunit.xml.dist ./phpunit.xml
```

Run the test suite:

```bash
./vendor/bin/phpunit
```

Or to run an individual test file:

```bash
./vendor/bin/phpunit tests/ClientTest.php
```

## Contributing

Feel free to make any comments, file issues or make pull requests.

## License

LearnyBox API Client for PHP is an open-sourced software licensed under the [MIT license](LICENSE).