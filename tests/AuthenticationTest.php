<?php

namespace LearnyBox\Tests;

use LearnyBox\Client;
use LearnyBox\Tests\AbstractTest;

/**
 * Class AuthenticationTest
 * @package LearnyBox\Tests
 */
class AuthenticationTest extends AbstractTest
{

    public function testAuthentication()
    {
        $client = Client::create($this->clientConfig);

        $result = $client->get('mail/contacts/');

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
    }

    /**
     * @depends testAuthentication
     */
    public function testRefreshAuthentication()
    {
        $client = Client::create($this->clientConfig);

        $result = $client->get('mail/contacts/');

        $this->assertNotNull($result);
        $this->assertTrue($result->status);

        $client->getOAuthStorage()->setAccessToken('this_token_does_not_exist');

        $result = $client->get('mail/contacts/');

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
    }

    /**
     * @depends testAuthentication
     */
    public function testAccessTokenIsKept()
    {
        $client1 = Client::create($this->clientConfig);
        $client2 = Client::create($this->clientConfig);

        $result1 = $client1->get('mail/contacts/');
        $accessToken1 = $client1->getOAuthStorage()->getAccessToken();

        $result2 = $client2->get('mail/contacts/');
        $accessToken2 = $client2->getOAuthStorage()->getAccessToken();

        $this->assertTrue($accessToken1 === $accessToken2);

    }

}