<?php

namespace LearnyBox\Tests;

use GuzzleHttp\ClientInterface;
use LearnyBox\Client;
use LearnyBox\OAuth\Storage\OAuthStorageInterface;

/**
 * Class ClientTest
 * @package LearnyBox\Tests
 */
class ClientTest extends AbstractTest
{

    public function testCreate()
    {
        $client = Client::create($this->clientConfig);

        $this->assertNotNull($client->getHttpClient());
        $this->assertInstanceOf(ClientInterface::class, $client->getHttpClient());

        $this->assertNotNull($client->getOAuthStorage());
        $this->assertInstanceOf(OAuthStorageInterface::class, $client->getOAuthStorage());
    }

    /**
     * @depends testCreate
     */
    public function testAll()
    {
        $client = Client::create($this->clientConfig);

        $result = $client->get('mail/contacts/');

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
    }

    /**
     * @depends testAll
     */
    public function testPost()
    {
        $client = Client::create($this->clientConfig);

        $result = $client->post('mail/contacts/', [
            'nom' => 'Doe',
            'prenom' => 'John',
            'email' => 'john.doe@mail.com'
        ]);

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
        $this->assertNotNull($result->data->id_contact);

        return $result->data->id_contact;
    }

    /**
     * @depends testPost
     */
    public function testPatch($id)
    {
        $client = Client::create($this->clientConfig);

        $result = $client->patch('mail/contacts/', [
            'id_contact' => $id,
            'new_email' => 'john.doe.bis@mail.com',
        ]);

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
    }

    /**
     * @depends testPost
     */
    public function testGet($id)
    {
        $client = Client::create($this->clientConfig);

        $result = $client->get('mail/contacts/' . $id . '/');

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
        $this->assertNotNull($result->data->id_contact);
    }

    /**
     * @depends testPost
     */
    public function testDelete($id)
    {
        $client = Client::create($this->clientConfig);

        $result = $client->delete('mail/contacts/' . $id . '/');

        $this->assertNotNull($result);
        $this->assertTrue($result->status);
    }

}