<?php

namespace LearnyBox\Tests;

use PHPUnit\Framework\TestCase;

abstract class AbstractTest extends TestCase
{

    /**
     * @var array
     */
    protected $clientConfig = [];


    public function setUp()
    {
        $this->clientConfig = [
            'api_key' => getenv('API_KEY'),
            'base_url' => getenv('BASE_URL'),
            'protocol' => getenv('PROTOCOL'),
            'subdomain' => getenv('SUBDOMAIN'),
            'timeout' => getenv('TIMEOUT')
        ];
    }

}